<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Database Servlet</title>
</head>
<body>
<!-- Retrieves the data from the servlet context -->
	<%
		ServletContext srvContext = getServletContext();
		String firstName = srvContext.getAttribute("firstName").toString();
		String lastName = srvContext.getAttribute("lastName").toString();
		String email = srvContext.getAttribute("email").toString();
		String contact = srvContext.getAttribute("contact").toString();
	%>
	
	<!-- Print the retrieved data -->
	<h1>User information has been stored at the database via JSP</h1>
	<p>First Name: <%=firstName %></p>
	<p>Last Name: <%=lastName %></p>
	<p>Email: <%=email %></p>
	<p>Contact Number: <%=contact %></p>


</body>
</html>