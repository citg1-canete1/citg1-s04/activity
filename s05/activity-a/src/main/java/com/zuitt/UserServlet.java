package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;

	public void init() throws ServletException{
		System.out.println("**************************************");
		System.out.println(" UserServlet has been initialized. ");
		System.out.println(" Connected to database. ");
		System.out.println("**************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		//Capture the data via the form inputs
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String email = req.getParameter("email");
		String contact = req.getParameter("contact");
		
		//Sets servlet context attributes to be consumed by the database servlet
		ServletContext srvContext = getServletContext();
		srvContext.setAttribute("firstName", firstName);
		srvContext.setAttribute("lastName", lastName);
		srvContext.setAttribute("email", email);
		srvContext.setAttribute("contact", contact);
		
		// forward the request and response to the database servlet.
		//res.sendRedirect("database");
		res.sendRedirect("database.jsp");
	}
	
	public void destroy(){
		System.out.println("**************************************");
		System.out.println(" UserServlet has been destroyed. ");
		System.out.println(" Disconnected to database. ");
		System.out.println("**************************************");
	}

	
}
