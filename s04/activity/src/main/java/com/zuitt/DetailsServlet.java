package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;

	private ArrayList<String> data = new ArrayList<>();
	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		
		//firstname
		String firstname = System.getProperty("firstname");
		
		//http session - lastname
		HttpSession session = req.getSession(); 
		String lastname = (String) session.getAttribute("lastname");
		
		//servletcontext - branding , email
		ServletContext srvContext = getServletContext();
		String branding = srvContext.getInitParameter("branding");
		String email = (String) srvContext.getAttribute("email");
		
		//contact
		String contact = req.getParameter("contact");
		
		PrintWriter output = res.getWriter();	
		output.println(
			"<h1>"+ branding + "</h1>" +
			"<p>First Name: " + firstname + "</p>" +
			"<p>Last Name: " + lastname + "</p>" +
			"<p>Contact: " + contact + "</p>" +
			"<p>Email: " + email + "</p>"
			);
		
    }
	

	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been destroy. ");
		System.out.println("******************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		
		RequestDispatcher rd = req.getRequestDispatcher("activity");
		rd.forward(req, res);
		
    }
	
}
