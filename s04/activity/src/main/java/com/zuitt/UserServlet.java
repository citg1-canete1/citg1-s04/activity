package com.zuitt;

import java.io.IOException;
import java.util.ArrayList;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	
	private ArrayList<String> data;
	private static final long serialVersionUID = 7560556849380195433L;

public void init() throws ServletException {
		
//		data = new ArrayList<>(Arrays.asList("Standard", "Deluxe"));
		System.out.println("******************************************");
		System.out.println(" UserServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" UserServlet has been destroy. ");
		System.out.println("******************************************");
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
//		firstname
		System.getProperties().put("firstname", req.getParameter("firstname"));
		
		//Http Session - lastname
		HttpSession session = req.getSession();
		session.setAttribute("lastname", req.getParameter("lastname"));
		
		//servletcontext - email
		ServletContext srvContext = getServletContext();
		srvContext.setAttribute("email", req.getParameter("email"));
		
		//sendRedirect - contact
		res.sendRedirect("details?contact="+ req.getParameter("contact"));
    }


}
